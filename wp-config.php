<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'carkod_com');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>8o}_+ph0:o}lZwN0L0n@cY<`4Se2J;6/7B]!|Hw/ :,KTIZ_KbT~%%XXQZBe)~*');
define('SECURE_AUTH_KEY',  'O]My;3r}$` hjZn0bIDp`mhs,50KX^-W9~4Mlb,7[6&Wu}5Eu>%93X_qXi.wa6Gg');
define('LOGGED_IN_KEY',    ']Qx:81I_I}+|fb/| S#s-l25_.b*m0SCV,vX/W?*.v!Eq)Zw?6?|hoa3o?IoPa?7');
define('NONCE_KEY',        '-[X!^M-I}E^nHC n-xy<!X9i<v]([h9t3/+`2j1~v7Pb+xj%i9-s&$vv2yDQEJ-~');
define('AUTH_SALT',        '?;iu+b<uI,9?/:CA@6KD1Y+)7Z<(TaF0+Hdi{EW3CJ%<Y:j+6^lPO&Ns-nKXxK-_');
define('SECURE_AUTH_SALT', 'J@qk;p]IUZjqG%uz|X0`ROA2=K*s-r bzG)2r{+{%5PjwxrE|<doU(*q+e>|A9nl');
define('LOGGED_IN_SALT',   '+lYn]8+$I0|:oAo<5!]!uqiG>1y<w]WFkkNaI;T]-sB-RP;s!W=E*5_[Y-;8OD~+');
define('NONCE_SALT',       'HvvH5vaE1AwL*T^Ddu?<1_CLlye2H^gQt/~d_=9@=sbg==`<*%AK>$As%s-@rMsF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_ec2cis_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
